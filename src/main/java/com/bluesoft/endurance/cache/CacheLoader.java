/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache;

import java.util.Map;
import java.util.Set;

/**
 * A strategy for loading a cache on demand.
 * <p>
 * @param <K> the key type
 * @param <V> the value type.
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
public interface CacheLoader<K, V> {

  /**
   * Load the value for a key.
   * <p>
   * @param key the key to load.
   * <p>
   * @return the value.
   */
  V load( K key );

  /**
   * Load values for multiple keys.
   * <p>
   * @param keys the keys to load
   * <p>
   * @return the values and keys.
   */
  Map<K, V> loadAll( Set<K> keys );
}
