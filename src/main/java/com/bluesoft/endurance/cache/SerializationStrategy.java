/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Defines a strategy pattern for serialization.
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
public interface SerializationStrategy {

  /**
   * Deserialize an object from the given input stream.
   * <p>
   * @param <T>   the type of the object.
   * @param steam the stream to read from
   * <p>
   * @return the deserialized object.
   */
  <T> T deserialize( InputStream steam );

  /**
   * Serialize and write the object to the given output stream
   * <p>
   * @param <T>    The type of the object being written
   * @param object the object to write.
   * @param stream the stream to write it too.
   */
  <T> void serialize( T object, OutputStream stream );
}
