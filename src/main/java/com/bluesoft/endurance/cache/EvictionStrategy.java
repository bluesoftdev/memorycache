/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache;

/**
 * Defines the interface for an Eviction strategy.
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
public interface EvictionStrategy {

  /**
   * Decorates a cache entry to add statistics that the eviction strategy needs to keep track of.
   * <p>
   * @param <K>   the key type
   * @param <V>   the value type
   * @param entry the entry to decorate.
   * <p>
   * @return the decorated entry.
   */
  <K, V> CacheEntry<K, V> decorate( CacheEntry<K, V> entry );

  /**
   * Called when the entry is accessed.
   * <p>
   * @param <K>   the key type
   * @param <V>   the value type
   * @param entry the entry that was accessed.
   */
  <K, V> void accessed( CacheEntry<K, V> entry );

  /**
   * Called to determine if an entry is eligible for eviction.
   * <p>
   * @param <K>   the key type
   * @param <V>   the value type
   * @param entry the entry to check. This is expected to be an entry that has been decorated with this evictor's entry decorator.
   * <p>
   * @return true if the entry should be evicted.
   */
  <K, V> boolean evict( CacheEntry<K, V> entry );

  /**
   * Carries out whatever operations are needed to make room in the cache for count more items to be added.
   * <p>
   * @param <K>   the key type
   * @param <V>   the value type
   * @param cache the cache to make room in.
   * @param count the count of room needed.
   * <p>
   * @return true if the strategy successfully removed enough elements to make room for the requested number of objects.
   */
  <K, V> boolean makeRoom( Cache<K, V> cache, int count );
}
