/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache.memory;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import com.bluesoft.endurance.cache.Cache;
import com.bluesoft.endurance.cache.CacheEntry;
import com.bluesoft.endurance.cache.CacheException;
import com.bluesoft.endurance.cache.CacheLoader;
import com.bluesoft.endurance.cache.EvictionStrategy;
import com.bluesoft.endurance.util.DefaultValueBasedReentrantLock;
import com.bluesoft.endurance.util.ValueBasedReentrantLock;

/**
 * A basic cache implementation for a memory based cache. Descendant classes must implement the {@link #createNewCacheEntry(java.lang.Object) }
 * function.
 * <p>
 * @param <K> the key type.
 * @param <V> the value type.
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
public abstract class AbstractMemoryCache<K, V> implements Cache<K, V> {

  // State Vars:
  protected Map<K, CacheEntry<K, V>> entries;
  protected EvictionStrategy evictor;
  protected CacheLoader<K, V> loader;
  private final ValueBasedReentrantLock<V> keyLocks = new DefaultValueBasedReentrantLock<>();
  // Configuration vars:
  protected String name;

  protected AbstractMemoryCache( final String name, AbstractMemoryCache<K, V> prototype ) {
    this( name, prototype.evictor, prototype.loader );
  }

  /**
   * Constructor that provides all the config/dependencies that are neede by an AbstractMemoryCache.
   * <p>
   * @param name    The name of the cache. This name is used for logging and other identification needs.
   * @param evictor The eviction strategy to use, may not be null.
   * @param loader  the loader to use if a requested item is not found, may be null.
   */
  public AbstractMemoryCache( final String name, final EvictionStrategy evictor, final CacheLoader loader ) {
    this.name = name;
    this.evictor = evictor;
    this.loader = loader;
    entries = new ConcurrentHashMap<>();
  }

  protected abstract CacheEntry<K, V> createNewCacheEntry( K key );

  /**
   * Gets the key locks manager.
   * <p>
   * @return the key locks manager.
   */
  ValueBasedReentrantLock getKeyLocks() {
    return keyLocks;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean containsKey( final K key ) {
    if ( key == null ) {
      throw new IllegalArgumentException( "key may not be null." );
    }
    return getEntry( key, false ) != null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public V get( final K key ) {
    if ( key == null ) {
      throw new IllegalArgumentException( "key may not be null." );
    }
    ReentrantLock lock = getKeyLocks().checkOutLock( key );
    lock.lock();
    try {
      CacheEntry<K, V> entry = getEntry( key, true );
      if ( entry != null ) {
        evictor.accessed( entry );
        return entry.getValue();
      }
      return null;
    } finally {
      lock.unlock();
      getKeyLocks().checkInLock( key );
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Map<K, V> getAll( final Set<? extends K> keys ) {
    return new AbstractMap<K, V>() {
      @Override
      public boolean containsKey( Object key ) {
        return (keys == null || keys.contains( (K)key )) && AbstractMemoryCache.this.containsKey( (K)key );
      }

      @Override
      public V get( Object key ) {
        return keys == null || keys.contains( (K)key ) ? AbstractMemoryCache.this.get( (K)key ) : null;
      }

      @Override
      public Set<Entry<K, V>> entrySet() {
        return new AbstractSet<Entry<K, V>>() {
          @Override
          public Iterator<Entry<K, V>> iterator() {
            return new Iterator<Entry<K, V>>() {
              Iterator<? extends K> keysItr = keys == null ? entries.keySet().iterator() : keys.iterator();

              @Override
              public boolean hasNext() {
                return keysItr.hasNext();
              }

              @Override
              public Entry<K, V> next() {
                return new Entry<K, V>() {
                  K key = keysItr.next();
                  V value = get( key );

                  @Override
                  public K getKey() {
                    return key;
                  }

                  @Override
                  public V getValue() {
                    return value;
                  }

                  @Override
                  public V setValue( V value ) {
                    throw new UnsupportedOperationException( "Not supported." );
                  }
                };
              }

              @Override
              public void remove() {
                throw new UnsupportedOperationException( "Not supported." );
              }
            };
          }

          @Override
          public int size() {
            return keys == null ? (int)AbstractMemoryCache.this.size() : keys.size();
          }
        };
      }
    };
  }

  protected CacheEntry<K, V> getEntry( final K key, final boolean needed ) {
    ReentrantLock lock = getKeyLocks().checkOutLock( key );
    lock.lock();
    try {
      CacheEntry<K, V> entry = entries.get( key );
      if ( entry == null && needed && loader != null ) {
        entry = loadEntry( key, null );
      } else if ( entry != null ) {
        final boolean shouldEvict = evictor.evict( entry );
        if ( shouldEvict ) {
          entry.release();
          if ( needed && loader != null ) {
            loadEntry( key, entry );
          } else {
            entries.remove( key );
            entry = null;
          }
        }
      }
      return entry;
    } finally {
      lock.unlock();
      getKeyLocks().checkInLock( key );
    }
  }

  private CacheEntry<K, V> loadEntry( final K key, CacheEntry<K, V> entry ) {
    if ( entry == null ) {
      entry = entries.get( key );
      if ( entry != null ) {
        return entry; // it was already loaded after the initial get.
      }
    }
    V value = loader.load( key );
    if ( entry == null ) {
      entry = evictor.decorate( createNewCacheEntry( key ) );
    }
    entry.setValue( value );
    entries.put( key, entry );
    return entry;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getName() {
    return name;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void put( final K key, final V value ) {
    if ( key == null ) {
      throw new IllegalArgumentException( "key may not be null." );
    }
    ReentrantLock lock = getKeyLocks().checkOutLock( key );
    lock.lock();
    try {
      CacheEntry<K, V> entry = getEntry( key, false );
      if ( entry == null ) {
        if ( !evictor.makeRoom( AbstractMemoryCache.this, 1 ) ) {
          throw new CacheException( "Cannot make room or any more elements." );
        }
        entry = evictor.decorate( createNewCacheEntry( key ) );
        entries.put( key, entry );
      }
      entry.setValue( value );
    } finally {
      lock.unlock();
      getKeyLocks().checkInLock( key );
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void putAll( Map<? extends K, ? extends V> map ) {
    map.entrySet().stream().forEach( (entry) -> {
      put( entry.getKey(), entry.getValue() );
    } );
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean remove( final K key ) {
    if ( key == null ) {
      throw new IllegalArgumentException( "key may not be null." );
    }
    ReentrantLock lock = getKeyLocks().checkOutLock( key );
    lock.lock();
    try {
      boolean removed = false;
      CacheEntry<K, V> entry = getEntry( key, false );
      if ( entry != null ) {
        entries.remove( key );
        entry.release();
        removed = true;
      }
      return removed;
    } finally {
      getKeyLocks().checkInLock( key );
      lock.unlock();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeAll( Set<? extends K> keys ) {
    keys.stream().forEach( (key) -> {
      remove( key );
    } );
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeAll() {
    removeAll( entries.keySet() );
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public long size() {
    return entries.size();
  }
}
