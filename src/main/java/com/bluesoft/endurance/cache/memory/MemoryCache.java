/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache.memory;

import com.bluesoft.endurance.cache.Cache;
import com.bluesoft.endurance.cache.CacheEntry;
import com.bluesoft.endurance.cache.CacheLoader;
import com.bluesoft.endurance.cache.EvictionStrategy;

/**
 * A concrete descendant of {@link AbstractMemoryCache} that implements an on heap cache.
 * <p>
 * @param <K> the key type.
 * @param <V> the value type.
 * <p>
 * @since 1.0.0
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 */
public class MemoryCache<K, V> extends AbstractMemoryCache<K, V> implements Cache<K, V> {

  /**
   * Creates a memory cache using the provided MemoryCache as a prototype.
   * <p>
   * @param name      the name of the new cache.
   * @param prototype the cache to copy parameters from.
   */
  public MemoryCache( String name, MemoryCache<K, V> prototype ) {
    super( name, prototype );
  }

  /**
   * Creates a new memory cache instance.
   * <p>
   * @param name    the name of the new cache.
   * @param evictor the eviction strategy to use for the cache.
   * @param loader  the cache loader to use for the cache.
   */
  public MemoryCache( String name, EvictionStrategy evictor, CacheLoader loader ) {
    super( name, evictor, loader );
  }

  /**
   * Overrides the {@link AbstractMemoryCache#createNewCacheEntry(java.lang.Object) } method to return a heap based implementation of CacheEntry.
   * <p>
   * @param key the key to create the entry for.
   * <p>
   * @return the new cache entry.
   */
  @Override
  protected CacheEntry<K, V> createNewCacheEntry( K key ) {
    return new MemoryCacheEntry<>( key, this );
  }
}
