/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache.memory;

import java.util.concurrent.locks.ReentrantLock;
import com.bluesoft.endurance.cache.CacheEntry;
import com.bluesoft.endurance.util.ValueBasedReentrantLock;

/**
 * Provides some basic functionality for a cache entry that resides in local memory. The actual storage mechanism for the key and value is not
 * specified. This class holds a reference to the containing cache and a lock which has been checked out of the {@link ValueBasedReentrantLock} with
 * which the key is used as the value.
 * <p>
 * @param <K> the key type
 * @param <V> the value type.
 * <p>
 * @since 1.0.0
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 */
public abstract class AbstractMemoryCacheEntry<K, V> implements CacheEntry<K, V> {

  private final AbstractMemoryCache<K, V> cache;
  private final ReentrantLock lock;

  /**
   * @param key   the key for this entry, this is not stored, it is used to acquire housekeeping information form the cache.
   * @param cache the cache this entry resides in.
   */
  public AbstractMemoryCacheEntry( K key, AbstractMemoryCache<K, V> cache ) {
    this.cache = cache;
    this.lock = cache.getKeyLocks().checkOutLock( key );
  }

  @Override
  public void release() {
    cache.getKeyLocks().checkInLock( getKey() );
  }

  public AbstractMemoryCache<K, V> getCache() {
    return cache;
  }

  public ReentrantLock getLock() {
    return lock;
  }
}
