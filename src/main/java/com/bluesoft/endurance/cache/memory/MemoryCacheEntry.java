/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache.memory;

import com.bluesoft.endurance.cache.CacheEntry;

/**
 * A memory based cache entry.
 * <p>
 * @param <K> the key type.
 * @param <V> the value type.
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
class MemoryCacheEntry<K, V> extends AbstractMemoryCacheEntry<K, V> implements CacheEntry<K, V> {

  private K key;
  private V value;

  /**
   * Create a new instance.
   * <p>
   * @param key   the key.
   * @param outer the containing cache.
   */
  public MemoryCacheEntry( K key, final MemoryCache<K, V> outer ) {
    super( key, outer );
    this.key = key;
  }

  /**
   * @see CacheEntry#getKey()
   */
  @Override
  public K getKey() {
    return key;
  }

  /**
   * @see CacheEntry#getValue()
   */
  @Override
  public V getValue() {
    return value;
  }

  /**
   * @see CacheEntry#setValue(java.lang.Object)
   */
  @Override
  public void setValue( V value ) {
    this.value = value;
  }

  /**
   * @see CacheEntry#release()
   */
  @Override
  public void release() {
    super.release();
    key = null;
    value = null;
  }
}
