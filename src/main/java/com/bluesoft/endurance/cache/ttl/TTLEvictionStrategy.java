/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache.ttl;

import java.util.concurrent.locks.ReentrantLock;

import com.bluesoft.endurance.cache.Cache;
import com.bluesoft.endurance.cache.CacheEntry;
import com.bluesoft.endurance.cache.EvictionStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A simple TTL based eviction strategy. Defines a number of maximum entries, and idleTTL and a total TTL.
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
public class TTLEvictionStrategy implements EvictionStrategy {

  private static final Logger LOG = LoggerFactory.getLogger( TTLEvictionStrategy.class );
  private final long idleTTL;
  private final long ttl;
  private final long maxElements;
  private final long maxEvictionTime = 100L;
  private final ReentrantLock llLock = new ReentrantLock();
  private CacheEntryDecorator first = null;
  private CacheEntryDecorator last = null;

  /**
   * Create a new instance specifying the TTL, idle TTL, and max elements.
   * <p>
   * @param ttl         the total time to live for an entry in the cache.
   * @param idleTTL     the time after an entry becomes idle, when it will be eligible for eviction.
   * @param maxElements the maximum number of elements in the cache.
   */
  public TTLEvictionStrategy( long ttl, long idleTTL, long maxElements ) {
    this.idleTTL = idleTTL;
    this.ttl = ttl;
    this.maxElements = maxElements;
  }

  /**
   * @see EvictionStrategy#evict(com.bluesoft.endurance.cache.CacheEntry)
   */
  @Override
  public <K, V> boolean evict( CacheEntry<K, V> _entry ) {
    CacheEntryDecorator<K, V> entry = (CacheEntryDecorator<K, V>)_entry;
    long now = System.currentTimeMillis();
    return now - entry.getLastAccessTime() > idleTTL || now - entry.getCreatedTime() > ttl;
  }

  /**
   * @see EvictionStrategy#decorate(com.bluesoft.endurance.cache.CacheEntry)
   */
  @Override
  public <K, V> CacheEntry<K, V> decorate( CacheEntry< K, V> _entry ) {
    llLock.lock();
    try {
      CacheEntryDecorator<K, V> entry = new CacheEntryDecorator<>( _entry );
      insertAtHead( entry );
      return entry;
    } finally {
      llLock.unlock();
    }
  }

  /**
   * @see EvictionStrategy#makeRoom(com.bluesoft.endurance.cache.Cache, int)
   */
  @Override
  public <K, V> boolean makeRoom( Cache<K, V> cache, int count ) {
    llLock.lock();
    try {
      LOG.trace( "making room for {} items in {}", count, cache.getName() );
      if ( cache.size() + count > maxElements ) {
        long spaceNeeded = cache.size() + count - maxElements;
        CacheEntryDecorator<K, V> entry = last;
        long start = System.currentTimeMillis();
        int removed = 0;
        while (entry != null && (removed < spaceNeeded || System.currentTimeMillis() - start < maxEvictionTime)) {
          if ( removed >= spaceNeeded && start - entry.getLastAccessTime() <= idleTTL && start - entry.getCreatedTime() <= ttl ) {
            break;
          }
          removed += 1;
          cache.remove( entry.getKey() );
          entry = entry.getPrev();
        }
        // If we did not remove enough entries due to expiry, remove the oldest entries until we have enough room.
        entry = last;
        while (removed < spaceNeeded && entry != null) {
          removed += 1;
          cache.remove( entry.getKey() );
          entry = entry.getPrev();
        }
        LOG.debug( "removed {} items to make room for {} in {}", removed, count, cache.getName() );
      }
      return cache.size() + count <= maxElements;
    } finally {
      llLock.unlock();
    }
  }

  /**
   * @see EvictionStrategy#accessed(com.bluesoft.endurance.cache.CacheEntry)
   */
  @Override
  public <K, V> void accessed( CacheEntry<K, V> _entry ) {
    CacheEntryDecorator<K, V> entry = (CacheEntryDecorator<K, V>)_entry;
    entry.setLastAccessTime( System.currentTimeMillis() );
    moveToHead( entry );
  }

  private void insertAtHead( CacheEntryDecorator entry ) {
    llLock.lock();
    try {
      entry.setNext( first );
      entry.setPrev( null );
      if ( first != null ) {
        first.setPrev( entry );
      } else {
        last = entry;
      }
      first = entry;
    } finally {
      llLock.unlock();
    }
  }

  private void removeFromList( CacheEntryDecorator entry ) {
    llLock.lock();
    try {
      if ( entry.getNext() != null ) {
        entry.getNext().setPrev( entry.getPrev() );
      } else {
        assert entry == last;
        last = entry.getPrev();
      }
      if ( entry.getPrev() != null ) {
        entry.getPrev().setNext( entry.getNext() );
      } else {
        assert entry == first;
        first = entry.getNext();
      }
      entry.setNext( null );
      entry.setPrev( null );
    } finally {
      llLock.unlock();
    }
  }

  private void moveToHead( CacheEntryDecorator entry ) {
    llLock.lock();
    try {
      if ( entry == first ) {
        return;
      }
      removeFromList( entry );
      entry.setNext( first );
      entry.setPrev( null );
      if ( first != null ) {
        first.setPrev( entry );
      }
      if ( last == null ) {
        assert first == null;
        last = entry;
      }
      first = entry;
    } finally {
      llLock.unlock();
    }
  }

  private boolean checkEntryList() {
    if ( first == null || last == null ) {
      return last == first;
    }
    CacheEntryDecorator entry = first;
    CacheEntryDecorator lst = null;
    while (entry != null) {
      lst = entry;
      entry = entry.getNext();
    }
    if ( last != lst ) {
      return false;
    }

    entry = last;
    lst = null;
    while (entry != null) {
      lst = entry;
      entry = entry.getPrev();
    }
    return first == lst;
  }

  private int entryListSize() {
    llLock.lock();
    try {
      CacheEntryDecorator entry = first;
      int count = 0;
      while (entry != null) {
        count += 1;
        entry = entry.getNext();
      }
      return count;
    } finally {
      llLock.unlock();
    }
  }

  private class CacheEntryDecorator<K, V> implements CacheEntry<K, V> {

    private CacheEntry<K, V> delegate;
    private long lastAccessTime;
    private long createdTime;
    private CacheEntryDecorator<K, V> next;
    private CacheEntryDecorator<K, V> prev;

    public CacheEntryDecorator( CacheEntry<K, V> delegate ) {
      this.delegate = delegate;
      lastAccessTime = createdTime = System.currentTimeMillis();
    }

    @Override
    public K getKey() {
      return delegate.getKey();
    }

    @Override
    public V getValue() {
      return delegate.getValue();
    }

    @Override
    public void setValue( V value ) {
      delegate.setValue( value );
    }

    @Override
    public void release() {
      removeFromList( this );
      delegate.release();
    }

    public long getLastAccessTime() {
      return lastAccessTime;
    }

    public void setLastAccessTime( long lastAccessTime ) {
      this.lastAccessTime = lastAccessTime;
    }

    public long getCreatedTime() {
      return createdTime;
    }

    public void setCreatedTime( long createdTime ) {
      this.createdTime = createdTime;
    }

    public CacheEntryDecorator<K, V> getNext() {
      return next;
    }

    public void setNext( CacheEntryDecorator<K, V> next ) {
      this.next = next;
    }

    public CacheEntryDecorator<K, V> getPrev() {
      return prev;
    }

    public void setPrev( CacheEntryDecorator<K, V> prev ) {
      this.prev = prev;
    }
  }
}
