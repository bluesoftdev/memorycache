/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A CacheLoader that is backed by another Cache implementation. This can be used to create chaining caches that allow for near caching in front of
 * more expensive caches.
 * <p>
 * @param <K> the key type.
 * @param <V> the value type.
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
public class BackingCacheLoader<K, V> implements CacheLoader<K, V> {

  private final Cache<K, V> backingCache;

  /**
   * Create a new BackingCacheLoader that uses the given cache to retrieve elements from.
   * <p>
   * @param backingCache the backing cache.
   */
  public BackingCacheLoader( Cache<K, V> backingCache ) {
    this.backingCache = backingCache;
  }

  /**
   * @see CacheLoader#load(java.lang.Object)
   * @param key the key to load
   * <p>
   * @return the loaded element, if it exists in the backing cache.
   */
  @Override
  public V load( K key ) {
    return backingCache.get( key );
  }

  /**
   * Loads all the requested keys by looping through them and calling {@link #load(java.lang.Object)}
   * <p>
   * @param keys the keys to load.
   * <p>
   * @return a map of the loaded keys/values.
   */
  @Override
  public Map<K, V> loadAll( Set<K> keys ) {
    Map<K, V> loaded = new HashMap<>();
    for ( K key : keys ) {
      V v = load( key );
      if ( v != null ) {
        loaded.put( key, v );
      }
    }
    return loaded;
  }
}
