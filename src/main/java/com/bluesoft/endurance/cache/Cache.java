/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache;

import java.util.Map;
import java.util.Set;

/**
 * A basic cache interface.
 * <p>
 * @param <K> the key type.
 * @param <V> the value type.
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
public interface Cache<K, V> {

  /**
   * The name of the cache.
   * <p>
   * @return the name of the cache as a string.
   */
  String getName();

  /**
   * Determines if the key is contained within the cache.
   * <p>
   * @param key the key.
   * <p>
   * @return true if the key is contained within the cache.
   */
  boolean containsKey( K key );

  /**
   * Get the value associated with the key.
   * <p>
   * @param key the key
   * <p>
   * @return the value that is associated with the key or null if the key is not in the cache.
   */
  V get( K key );

  /**
   * Get a map that is backed by the cache and contains the given keys. The size of the map will be the size of the keys {@link Set} and the map will
   * contain all of the elements in keys, however it may return null for keys that are not actually in the backing cache. Since the map is backed by
   * the cache, changes in the state of entries will be reflected. E.g. a get(k) that returns a value may later return null if the entry expires or is
   * removed by another operation.
   * <p>
   * @param keys the keys to get. if keys == null, returns a map view of the entire cache.
   * <p>
   * @return the map giving access to the keys requested.
   */
  Map<K, V> getAll( Set<? extends K> keys );

  /**
   * Put a value into the cache.
   * <p>
   * @param key   the key for the value.
   * @param value the value.
   */
  void put( K key, V value );

  /**
   * Put all the given key/value pairs into the cache.
   * <p>
   * @param values the values to put.
   */
  void putAll( Map<? extends K, ? extends V> values );

  /**
   * Remove the value for the given key.
   * <p>
   * @param key the key.
   * <p>
   * @return true if the key was found and removed.
   */
  boolean remove( K key );

  /**
   * Removes all the values for the given keys.
   * <p>
   * @param keys the keys.
   */
  void removeAll( Set<? extends K> keys );

  /**
   * Clears the cache of all values.
   */
  void removeAll();

  /**
   * @return an approximation of the number of elements in the cache.
   */
  long size();
}
