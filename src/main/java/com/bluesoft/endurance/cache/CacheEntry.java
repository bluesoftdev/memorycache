/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache;

/**
 * Represents a cache entry. Not generally useful to users of the cache class but used by the providers to interact between cache implementations and
 * eviction strategies.
 * <p>
 * @param <K> the key type.
 * @param <V> the value type.
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
public interface CacheEntry<K, V> {

  /**
   * @return the key value.
   */
  K getKey();

  /**
   * @return the value.
   */
  V getValue();

  /**
   * Set the value.
   * <p>
   * @param value the value.
   */
  void setValue( V value );

  /**
   * Release resources associated with this cache entry.
   */
  void release();
}
