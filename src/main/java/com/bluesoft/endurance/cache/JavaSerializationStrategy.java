/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * implements a basic Java object serialization implementation of the {@link SerializationStrategy} interface.
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
public class JavaSerializationStrategy implements SerializationStrategy {

  @Override
  public <T> T deserialize( InputStream steam ) {
    try ( ObjectInputStream ois = new ObjectInputStream( steam ) ) {
      return (T)ois.readObject();
    } catch ( IOException ex ) {
      throw new CacheException( "IOException while deserializing data from the cache.", ex );
    } catch ( ClassNotFoundException ex ) {
      throw new CacheException( "ClassNotFoundException while deserializing data from the cache.", ex );
    }
  }

  @Override
  public <T> void serialize( T object, OutputStream stream ) {
    try ( ObjectOutputStream oos = new ObjectOutputStream( stream ) ) {
      oos.writeObject( object );
    } catch ( IOException ex ) {
      throw new CacheException( "IOException while deserializing data from the cache.", ex );
    }
  }
}
