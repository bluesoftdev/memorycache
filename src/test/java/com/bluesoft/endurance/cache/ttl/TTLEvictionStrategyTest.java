/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache.ttl;

import com.bluesoft.endurance.cache.Cache;
import com.bluesoft.endurance.cache.CacheEntry;
import org.testng.annotations.Test;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.easymock.EasyMock.verify;
import static org.testng.Assert.assertTrue;

/**
 *
 * @author psimerd
 */
public class TTLEvictionStrategyTest {

  @Test
  public void testMakeRoomNotFull() {
    TTLEvictionStrategy strategy = new TTLEvictionStrategy( 1, 1, 10 );
    Cache cache = createMock( Cache.class );
    expect( cache.getName() ).andStubReturn( "test" );
    expect( cache.size() ).andStubReturn( 5L );
    replay( cache );
    assert strategy.makeRoom( cache, 1 ) : "unable to make room, when it should";
    verify( cache );
    reset( cache );
  }

  @Test
  public void testMakeRoomFull() throws Exception {
    TTLEvictionStrategy strategy = new TTLEvictionStrategy( 1, 1, 5 );
    for ( int i = 0; i < 5; i++ ) {
      strategy.decorate( new FakeCacheEntry() );
    }
    Thread.sleep( 2 ); // wait till everything is expired
    Cache cache = createMock( Cache.class );
    expect( cache.getName() ).andStubReturn( "test" );
    expect( cache.size() ).andReturn( 5L ).times( 2 ).andReturn( 2L ).once();
    expect( cache.remove( "key" ) ).andStubReturn( Boolean.TRUE );
    replay( cache );
    assertTrue( strategy.makeRoom( cache, 1 ), "unable to make room, when it should" );
    verify( cache );
    reset( cache );
  }

  private static class FakeCacheEntry implements CacheEntry {

    public FakeCacheEntry() {
    }

    @Override
    public Object getKey() {
      return "key";
    }

    @Override
    public Object getValue() {
      throw new UnsupportedOperationException( "Not supported yet." );
    }

    @Override
    public void setValue( Object value ) {
      throw new UnsupportedOperationException( "Not supported yet." );
    }

    @Override
    public void release() {
      throw new UnsupportedOperationException( "Not supported yet." );
    }
  }
}
