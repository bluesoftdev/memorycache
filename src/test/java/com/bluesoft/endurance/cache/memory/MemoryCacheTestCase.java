/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache.memory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.bluesoft.endurance.cache.Cache;
import com.bluesoft.endurance.cache.CacheEntry;
import com.bluesoft.endurance.cache.CacheException;
import com.bluesoft.endurance.cache.CacheLoader;
import com.bluesoft.endurance.cache.EvictionStrategy;
import com.bluesoft.endurance.cache.ttl.TTLEvictionStrategy;
import org.easymock.Capture;
import org.easymock.IAnswer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.easymock.EasyMock.same;
import static org.easymock.EasyMock.verify;

import static org.testng.Assert.*;

/**
 *
 * @author psimerd
 */
public class MemoryCacheTestCase {

  private static final Logger LOG = LoggerFactory.getLogger( MemoryCacheTestCase.class );
  private Cache<String, String> cache;

  @BeforeClass
  public void createCache() {
    cache = new MemoryCache<>( "test", new TTLEvictionStrategy( 1000, 1000, 5 ), null );
  }

  @Test( groups = "put" )
  public void testPut() {
    cache.put( "foo", "snafu" );
    cache.put( "bar", "foobar" );
    cache.put( "fubar", "foo" );
  }

  @Test( groups = "put", dependsOnMethods = "testPut" )
  public void testPutAll() {
    Map<String, String> all = new LinkedHashMap<>();
    all.put( "snafu", "bar" );
    all.put( "foobar", "fubar" );
    all.put( "daisy", "duck" );
    all.put( "donald", "duck" );
    cache.putAll( all );
  }

  @Test( groups = "access", dependsOnGroups = "put" )
  public void testGetCacheFull() {
    assertNull( cache.get( "foo" ) );
    assertNull( cache.get( "bar" ) );
    assertEquals( cache.get( "fubar" ), "foo" );
    assertEquals( cache.get( "snafu" ), "bar" );
    assertEquals( cache.get( "foobar" ), "fubar" );
    assertEquals( cache.get( "daisy" ), "duck" );
    assertEquals( cache.get( "donald" ), "duck" );
  }

  @Test( expectedExceptions = IllegalArgumentException.class )
  public void testPutNull() {
    cache.put( null, "foo" );
  }

  @Test( expectedExceptions = IllegalArgumentException.class )
  public void testGetNull() {
    cache.get( null );
  }

  @Test( expectedExceptions = IllegalArgumentException.class )
  public void testContainsKeyNull() {
    cache.containsKey( null );
  }

  @Test( expectedExceptions = IllegalArgumentException.class )
  public void testRemoveNull() {
    cache.remove( null );
  }

  @Test( groups = "access", dependsOnGroups = "put" )
  public void testGetAll() {
    Map<String, String> all = cache.getAll( new HashSet<>( Arrays.asList( "fubar", "snafu", "foo", "daisy" ) ) );
    assertEquals( all.size(), 4 );
    assertTrue( all.containsKey( "fubar" ) );
    assertFalse( all.containsKey( "bar" ) );
    assertEquals( all.get( "fubar" ), "foo" );
    assertEquals( all.get( "snafu" ), "bar" );
    assertEquals( all.get( "daisy" ), "duck" );
    assertNull( all.get( "foo" ) );
    final Set<Entry<String, String>> entries = all.entrySet();
    assertNotNull( entries );
    Iterator<Map.Entry<String, String>> mapIterator = entries.iterator();
    assertNotNull( mapIterator );
    int count = 0;
    while (mapIterator.hasNext()) {
      Map.Entry<String, String> entry = mapIterator.next();
      assertNotNull( entry );
      if ( entry.getKey().equals( "foo" ) ) {
        assertNull( entry.getValue() );
      } else {
        assertNotNull( entry.getValue() );
      }
      try {
        entry.setValue( "foobar" );
        fail();
      } catch ( UnsupportedOperationException ex ) {
        LOG.info( "got expected exception: ", ex );
      }
      try {
        mapIterator.remove();
        fail();
      } catch ( UnsupportedOperationException ex ) {
        LOG.info( "got expected exception: ", ex );
      }
      count += 1;
    }
    assertEquals( count, 4 );
  }

  @Test( groups = "access", dependsOnGroups = "put" )
  public void testGetAllNull() {
    Map<String, String> all = cache.getAll( null );
    assertEquals( all.size(), 5 );
    assertTrue( all.containsKey( "fubar" ) );
    assertFalse( all.containsKey( "bar" ) );
    assertEquals( all.get( "fubar" ), "foo" );
    assertEquals( all.get( "snafu" ), "bar" );
    assertEquals( all.get( "daisy" ), "duck" );
    assertNull( all.get( "foo" ) );
    final Set<Entry<String, String>> entries = all.entrySet();
    assertNotNull( entries );
    Iterator<Map.Entry<String, String>> mapIterator = entries.iterator();
    assertNotNull( mapIterator );
    int count = 0;
    while (mapIterator.hasNext()) {
      Map.Entry<String, String> entry = mapIterator.next();
      assertNotNull( entry );
      if ( entry.getKey().equals( "foo" ) ) {
        assertNull( entry.getValue() );
      } else {
        assertNotNull( entry.getValue() );
      }
      try {
        entry.setValue( "foobar" );
        fail();
      } catch ( UnsupportedOperationException ex ) {
        LOG.info( "got expected exception: ", ex );
      }
      try {
        mapIterator.remove();
        fail();
      } catch ( UnsupportedOperationException ex ) {
        LOG.info( "got expected exception: ", ex );
      }
      count += 1;
    }
    assertEquals( count, 5 );
  }

  @Test( groups = "deletion", dependsOnGroups = "access" )
  public void testRemove() {
    cache.remove( "foobar" );
    assertEquals( cache.size(), 4 );
    assertFalse( cache.containsKey( "foobar" ) );
  }

  @Test( groups = "deletion", dependsOnGroups = "access", dependsOnMethods = "testRemove" )
  public void testRemoveAllSet() {
    cache.removeAll( new HashSet<>( Arrays.asList( "fubar", "snafu" ) ) );
    assertEquals( cache.size(), 2 );
    assertFalse( cache.containsKey( "fubar" ) );
    assertFalse( cache.containsKey( "snafu" ) );
  }

  @Test( groups = "deletion", dependsOnGroups = "access", dependsOnMethods = "testRemoveAllSet" )
  public void testRemoveAll() {
    cache.removeAll();
    assertEquals( cache.size(), 0 );
  }

  @Test( groups = "prototype", dependsOnGroups = "put" )
  public void testPrototype() {
    Cache<String, String> cache2 = new MemoryCache<>( "test2", (MemoryCache<String, String>)cache );
    assertNotNull( cache2 );
  }

  @Test
  public void testCacheWithLoader() throws Exception {
    EvictionStrategy mockEvictor = createMock( EvictionStrategy.class );
    final Map<String, String> backer = new HashMap<>();
    backer.put( "foo", "snafu" );
    backer.put( "bar", "fubar" );
    backer.put( "baz", "foobar" );
    final Cache<String, String> testCache = new MemoryCache<>( "testCacheWithLoader", mockEvictor, new CacheLoader<String, String>() {

      @Override
      public String load( String key ) {
        return backer.get( key );
      }

      @Override
      public Map<String, String> loadAll( Set<String> keys ) {
        Map<String, String> ret = new LinkedHashMap<>( keys.size() );
        for ( String key : keys ) {
          if ( backer.containsKey( key ) ) {
            ret.put( key, backer.get( key ) );
          }
        }
        return ret;
      }

    } );

    expect( mockEvictor.makeRoom( same( testCache ), eq( 1 ) ) ).andStubReturn( Boolean.TRUE );
    final Capture<CacheEntry> decorateCapture = new Capture<>();
    expect( mockEvictor.decorate( capture( decorateCapture ) ) ).andStubAnswer( new IAnswer<CacheEntry>() {

      @Override
      public CacheEntry answer() throws Throwable {
        return decorateCapture.getValue();
      }
    } );
    mockEvictor.accessed( anyObject( CacheEntry.class ) );
    expectLastCall().times( 5 );
    expect( mockEvictor.evict( anyObject( CacheEntry.class ) ) ).andReturn( Boolean.FALSE ).times( 1 );
    expect( mockEvictor.evict( anyObject( CacheEntry.class ) ) ).andReturn( Boolean.TRUE ).times( 1 );
    replay( mockEvictor );

    assertEquals( testCache.size(), 0 );
    assertEquals( testCache.get( "foo" ), "snafu" );
    assertEquals( testCache.size(), 1 );
    assertEquals( testCache.get( "bar" ), "fubar" );
    assertEquals( testCache.size(), 2 );
    assertEquals( testCache.get( "baz" ), "foobar" );
    assertEquals( testCache.size(), 3 );
    testCache.remove( "bar" );
    assertEquals( testCache.get( "bar" ), "fubar" );
    assertFalse( testCache.containsKey( "baz" ) );
    assertEquals( testCache.get( "baz" ), "foobar" );
    verify( mockEvictor );

    reset( mockEvictor );
    expect( mockEvictor.makeRoom( same( testCache ), eq( 1 ) ) ).andStubReturn( Boolean.FALSE );
    replay( mockEvictor );
    try {
      testCache.put( "fubar", "snafu" );
      fail();
    } catch ( CacheException ex ) {
      LOG.info( "got expected exception:", ex );
    }
    verify( mockEvictor );
  }
}
