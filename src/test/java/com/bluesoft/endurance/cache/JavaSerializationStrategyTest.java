/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import org.testng.annotations.Test;

import static org.easymock.EasyMock.anyInt;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

/**
 * Test case for {@link JavaSerializationStrategy}
 * <p>
 * @author danap@bluesoftdev.com &lt;Dana H. P'Simer&gt;
 * @since 1.0.0
 */
public class JavaSerializationStrategyTest {

  private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger( JavaSerializationStrategyTest.class );

  private static class TestBean implements Serializable {

    private int integer;
    private long longInt;
    private String str;
    private boolean b;

    public int getInteger() {
      return integer;
    }

    public void setInteger( int integer ) {
      this.integer = integer;
    }

    public long getLongInt() {
      return longInt;
    }

    public void setLongInt( long longInt ) {
      this.longInt = longInt;
    }

    public String getStr() {
      return str;
    }

    public void setStr( String str ) {
      this.str = str;
    }

    public boolean isB() {
      return b;
    }

    public void setB( boolean b ) {
      this.b = b;
    }

  }

  @Test
  public void testHappy() {
    TestBean testBean = new TestBean();
    testBean.setB( true );
    testBean.setInteger( 5 );
    testBean.setLongInt( 55L );
    testBean.setStr( "the quick brown fox jumped over the lazy dogs." );

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    JavaSerializationStrategy serializer = new JavaSerializationStrategy();
    serializer.serialize( testBean, baos );
    ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray() );
    TestBean testBeanDeserialized = serializer.deserialize( bais );

    assertEquals( testBeanDeserialized.isB(), testBean.isB() );
    assertEquals( testBeanDeserialized.getInteger(), testBean.getInteger() );
    assertEquals( testBeanDeserialized.getLongInt(), testBean.getLongInt() );
    assertEquals( testBeanDeserialized.getStr(), testBean.getStr() );
  }

  @Test
  public void testSerializeIOException() throws Exception {
    OutputStream mockOS = createMock( OutputStream.class );
    mockOS.write( anyObject( byte[].class ), eq( 0 ), anyInt() );
    expectLastCall().andThrow( new IOException( "mocked exception" ) );

    replay( mockOS );
    JavaSerializationStrategy serializer = new JavaSerializationStrategy();
    try {
      serializer.serialize( new TestBean(), mockOS );
      fail();
    } catch ( CacheException ex ) {
      LOG.info( "got expected exception: ", ex );
    }
    verify( mockOS );
  }

  @Test
  public void testDeserializeIOException() throws Exception {
    InputStream mockIS = createMock( InputStream.class );
    expect( mockIS.read( anyObject( byte[].class ), eq( 0 ), anyInt() ) ).andThrow( new IOException( "mocked exception" ) );

    replay( mockIS );
    JavaSerializationStrategy serializer = new JavaSerializationStrategy();
    try {
      serializer.deserialize( mockIS );
      fail();
    } catch ( CacheException ex ) {
      LOG.info( "got expected exception: ", ex );
    }
    verify( mockIS );
  }
}
