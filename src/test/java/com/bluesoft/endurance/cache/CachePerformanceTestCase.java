/*
 * Copyright 2014 Dana H. P'Simer & BluesSoft Development, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluesoft.endurance.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.cache.Caching;

import com.bluesoft.endurance.cache.memory.MemoryCache;
import com.bluesoft.endurance.cache.ttl.TTLEvictionStrategy;
import com.bluesoft.endurance.instrumentation.time.Timing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

/**
 *
 * @author psimerd
 */
public class CachePerformanceTestCase {

  private static final boolean ENABLED = true;
  private static final Logger LOG = LoggerFactory.getLogger( CachePerformanceTestCase.class );
  public static final String MEM_PERFORMANCE = "mem_performance";
  public static final String EH_PERFORMANCE = "eh_performance";
  protected Random rand = new Random();
  protected Cache<UUID, byte[]> cache = null;
  protected List<UUID> keys;
  protected Map<UUID, byte[]> keyValues = new HashMap<>();
  protected Timing putTimings;
  protected Timing getTimings;
  protected static final int THREADS = 2;
  protected static final int COUNT = 600;
  protected static final boolean ehcache_enabled = ENABLED && true;

  @BeforeGroups( value = { MEM_PERFORMANCE }, enabled = ENABLED )
  public void createMemCache() {
    LOG.trace( "in createMemCache" );
    cache = new MemoryCache<>( "test", new TTLEvictionStrategy( 10000L, 10000L, 200 ), null );
    keyValues.clear();
    putTimings = new Timing();
    getTimings = new Timing();
  }

  @BeforeGroups( value = { EH_PERFORMANCE }, enabled = ehcache_enabled )
  public void createEhCache() {
    LOG.trace( "in createEhCache" );
    cache = new Cache() {
      private final javax.cache.Cache inner = Caching.getCacheManager().<UUID, byte[]>getCache( "test" );

      @Override
      public String getName() {
        return inner.getName();
      }

      @Override
      public boolean containsKey( Object key ) {
        return inner.containsKey( key );
      }

      @Override
      public Object get( Object key ) {
        return inner.get( key );
      }

      @Override
      public Map getAll( Set keys ) {
        return inner.getAll( keys );
      }

      @Override
      public void put( Object key, Object value ) {
        inner.put( key, value );
      }

      @Override
      public void putAll( Map values ) {
        inner.putAll( values );
      }

      @Override
      public boolean remove( Object key ) {
        return inner.remove( key );
      }

      @Override
      public void removeAll( Set keys ) {
        inner.removeAll( keys );
      }

      @Override
      public void removeAll() {
        inner.removeAll();
      }

      @Override
      public long size() {
        return 0;
      }
    };
    assert cache != null;
    keyValues.clear();
    putTimings = new Timing();
    getTimings = new Timing();
  }

  @BeforeGroups( value = { MEM_PERFORMANCE, EH_PERFORMANCE }, enabled = ENABLED )
  public void createKeys() {
    LOG.trace( "in createKeys" );
    keys = new ArrayList<>( 1000 );
    for ( int i = 0; i < 1000; i++ ) {
      keys.add( UUID.randomUUID() );
    }
  }

  @Test( threadPoolSize = THREADS, invocationCount = COUNT, groups = { MEM_PERFORMANCE }, enabled = ENABLED )
  public void testMemPerformance() throws Exception {
    LOG.trace( "in testMemPerformance" );
    testPerf();
  }

  @Test( threadPoolSize = THREADS, invocationCount = COUNT, groups = { EH_PERFORMANCE }, enabled = ehcache_enabled )
  public void testEhPerformance() throws Exception {
    LOG.trace( "in testEhPerformance" );
    testPerf();
  }

  protected void testPerf() {
    final UUID key = keys.get( rand.nextInt( 1000 ) );
    if ( rand.nextInt( 100 ) < 50 ) {
      final byte[] value = new byte[(int)(1024 * Math.abs( rand.nextGaussian() ))];
      rand.nextBytes( value );
      putTimings.time( () -> {
        cache.put( key, value );
      } );
      keyValues.put( key, value );
    } else {
      getTimings.time( () -> cache.get( key ) );
    }
  }

  private void printStats( Timing timing ) {
    LOG.info( "\n\tcount = " + timing.getCount() + "\n\taverage = " + timing.getAverage() + "\n\tstandard deviation = " + timing.
            getStandardDev() );
  }

  protected void printTimings() {
    LOG.info( "put: " );
    printStats( putTimings );
    LOG.info( "get: " );
    printStats( getTimings );
  }

  @AfterGroups( MEM_PERFORMANCE )
  public void printMemStatistics() {
    LOG.info( "Mem Timings:" );
    printTimings();
  }

  @AfterGroups( value = EH_PERFORMANCE, enabled = ehcache_enabled )
  public void printEHStatistics() {
    LOG.info( "EHC Timings:" );
    printTimings();
  }
}
